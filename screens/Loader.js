import React, { Component } from "react";
import { View } from "react-native";
import SleekLoadingIndicator from "../screens/SleekLoadingIndicator";

class Loader extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          position: "absolute",
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          backgroundColor: "transparent",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            width: "100%",
            height: 100,
          }}
        >
          <SleekLoadingIndicator
            style={{ backgroundColor: "white" }}
            text="Loading... Please wait"
            loading={true}
          />
        </View>
      </View>
    );
  }
}
export default Loader;
