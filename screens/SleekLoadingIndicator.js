import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ActivityIndicator
} from 'react-native';

class SleekLoadingIndicator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: 'Loading...',
            loading: true
        };
    }
    render() {
        if (this.props.loading) {
            return (
                <View style={styles.container}>
                    <LoadingIndicator text={this.props.text} />
                </View>
            );
        } else {
            return null;
        }
    }
}

class LoadingIndicator extends Component {
    render() {
        return (
            <View style={styles.loadingContainer}>
                <View style={styles.spinnerContainer}>
                    <ActivityIndicator
                        animating={true}
                        size='large'
                        color="rgb(14,106,72)"
                    />
                </View>
                <Text style={styles.loadingText}>{this.props.text}</Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    loadingContainer: {
        backgroundColor: 'transparent',
        borderRadius: 8,
        opacity: .8,
        justifyContent: 'center',
        alignItems: 'center',

    },

    loadingText: {
        fontWeight: 'bold',
        color: '#217DB9',
        textAlign: 'center',
        fontSize: 14,
    },

    spinnerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 4,
        marginTop: 4,
    },
});

export default SleekLoadingIndicator
