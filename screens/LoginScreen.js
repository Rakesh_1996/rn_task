import React, { Component } from "react";
import { connect } from "react-redux";
import {
  YellowBox,
  Text,
  View,
  TextInput,
  ImageBackground,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";

import Styles from "../styles/LoginScreen";
import Loader from "../screens/Loader";

const reg_mail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,10}$/;

YellowBox.ignoreWarnings(["Warning: ReactNative.createElement"]);
console.disableYellowBox = true;

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      userName: "",
      password: "",
      emailError: "",
      passwordError: "",
    };
  }
  handleChange = (text, type) => {
    if (type === "userName") {
      if (reg_mail.test(text) === true) {
        let emailChange = text.toLowerCase();
        this.setState({
          userName: emailChange,
          emailError: "",
        });
      } else {
        if (!text.length) {
          this.setState({
            userName: text,
            emailError: "Enter Email",
          });
        } else {
          this.setState({
            userName: text,
            emailError: "Enter valid Email",
          });
        }
      }
    }
    if (type === "password") {
      if (text) {
        if (text.length >= 6) {
          this.setState({
            password: text,
            passwordError: "",
          });
        } else {
          this.setState({
            password: text,
            passwordError: "Password must contain 6 Characters",
          });
        }
      } else {
        this.setState({
          password: text,
          passwordError: "Enter Password",
        });
      }
    }
  };

  validate = () => {
    if (!this.state.userName.length) {
      this.setState({ emailError: "Enter Email" });
      return;
    }
    if (!this.state.password.length) {
      this.setState({ passwordError: "Enter Password" });
      return;
    }
    this.onSubmitData();
  };

  onSubmitData = () => {
    this.setState({ loading: false });
    const { userName = "", password: statePassword = "" } = this.state;
    const { username = "", password = "" } = this.props.credentails;
    if (userName === username && statePassword === password) {
      this.setState({ loading: false });
      this.props.navigation.navigate("App");
    } else {
      this.setState({ emailError: "Invalid Credentails", loading: false });
    }
  };
  render() {
    return (
      <View style={Styles.mainView}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="#354588"
        />
        <View style={Styles.secondaryView}>
          <Text style={Styles.loginText}>Login</Text>
          <View style={Styles.userView}>
            <TextInput
              style={Styles.TextInput}
              placeholder="User Name"
              value={this.state.userName}
              placeholderTextColor="#676A74"
              autoCapitalize="none"
              keyboardType="email-address"
              fontSize={16}
              underlineColorAndroid="white"
              returnKeyType={"next"}
              onChangeText={(text) => this.handleChange(text, "userName")}
            />
          </View>
          <Text style={Styles.errormsgs}>
            {this.state.emailError ? this.state.emailError : null}
          </Text>
          <View style={Styles.passwordView}>
            <TextInput
              placeholder="Password"
              value={this.state.password}
              fontSize={16}
              placeholderTextColor="#676A74"
              underlineColorAndroid="white"
              secureTextEntry={true}
              returnKeyType={"done"}
              style={Styles.TextInput}
              onChangeText={(text) => this.handleChange(text, "password")}
            />
          </View>
          <Text style={Styles.errormsgs}>
            {this.state.passwordError ? this.state.passwordError : null}
          </Text>
          <TouchableOpacity
            activeOpacity={0.7}
            style={Styles.loginButton}
            onPress={() => this.validate()}
          >
            <Text style={Styles.loginButtonText}>Login</Text>
          </TouchableOpacity>
        </View>

        {this.state.loading == true ? <Loader /> : null}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    credentails: state.credentails.credentails,
  };
}
export default connect(mapStateToProps)(LoginScreen);
