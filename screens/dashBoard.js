import React, { Component } from "react";
import { View, Text, StatusBar, FlatList } from "react-native";
import { connect } from "react-redux";
import Loader from "./Loader";
import Styles from "../styles/dashboard";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      usersList: [],
    };
  }
  static navigationOptions = () => {
    return {
      title: `Employee List`,
    };
  };
  componentDidMount = () => {
    this.setState({ usersList: this.props.users, loading: false });
  };
  render() {
    return (
      <View style={Styles.mainView}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="#354588"
        />
        {this.stateloading ? (
          <Loader />
        ) : (
          <FlatList
            data={this.state.usersList}
            width="100%"
            extraData={this.state.usersList}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => <UserTable userData={item} />}
          />
        )}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    users: state.users.users,
  };
}
export default connect(mapStateToProps)(Dashboard);

const UserTable = (props) => {
  return (
    <View style={Styles.userView}>
      <View style={Styles.userTable}>
        <Text style={Styles.userLabel}>Name :</Text>
        <Text style={Styles.userValue}>{props.userData.name}</Text>
      </View>
      <View style={Styles.userTable}>
        <Text style={Styles.userLabel}>Email :</Text>
        <Text style={Styles.userValue}>{props.userData.email}</Text>
      </View>
      <View style={Styles.userTable}>
        <Text style={Styles.userLabel}>Gender :</Text>
        <Text style={Styles.userValue}>{props.userData.gender}</Text>
      </View>
      <View style={Styles.userTable}>
        <Text style={Styles.userLabel}>Age :</Text>
        <Text style={Styles.userValue}>{props.userData.age} Yrs</Text>
      </View>
      <View style={Styles.userTable}>
        <Text style={Styles.userLabel}>Phone :</Text>
        <Text style={Styles.userValue}>{props.userData.phoneNo}</Text>
      </View>
    </View>
  );
};
