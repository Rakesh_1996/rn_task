import { Platform } from "react-native";
var _AppConstants = {
  ANDROID: {
    Font_Size_Extra_small: 8,
    Font_Size_Small: 10,
    Font_Size_Medium: 12,
    Font_Size_Large: 14,
    Font_Size_Extra_Large: 16,
    Font_Size_Double_Extra_Large: 20,
  },
  IOS: {
    Font_Size_Extra_small: 10,
    Font_Size_Small: 12,
    Font_Size_Medium: 14,
    Font_Size_Large: 16,
    Font_Size_Extra_Large: 18,
    Font_Size_Double_Extra_Large: 22,
  },
};
function getPlatform() {
  return Platform.OS === "android" ? "ANDROID" : "IOS";
}
function getEnvironment() {
  var platform = getPlatform();
  return _AppConstants[platform];
}

var Environment = getEnvironment();
module.exports = Environment;

export { _AppConstants };
