import Credentails from '../assets/json/credentails.json'
const credentails = (state = {credentails:Credentails}, action) => {
  switch (action.type) {
    case "CRED":
      return Object.assign({}, state, {
        credentails: action.credentails,
      });
    default:
      return state;
  }
};

export default credentails;
