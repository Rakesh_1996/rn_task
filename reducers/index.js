import { combineReducers } from "redux";
import CredentailsReducer from "./credentails";
import UsersReducer from "./users";
//import and add more child reducers as your project builds.
export default combineReducers({
  credentails: CredentailsReducer,
  users: UsersReducer,
});
