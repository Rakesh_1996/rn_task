import Users from "../assets/json/users.json";
const users = (state = { users: Users.user }, action) => {
  switch (action.type) {
    case "USERS":
      return Object.assign({}, state, {
        users: action.users,
      });
    default:
      return state;
  }
};

export default users;
