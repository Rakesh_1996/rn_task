import AppConstants from "../util/AppConstants";

export default {
  mainView: {
    flex: 1,
    backgroundColor: "#FFF",
    paddingBottom: 50,
  },
  userView: {
    flex: 1,
    backgroundColor: "skyblue",
    margin: 5,
    borderRadius: 5,
    padding: 10,
  },
  userTable: {
    flex: 1,
    flexDirection: "row",
  },
  userLabel: {
    fontSize: 20,
    color: "white",
    paddingRight: 10,
  },
  userValue: {
    fontSize: 20,
    color: "white",
  },
};
