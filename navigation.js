import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import LoginScreen from "./screens/LoginScreen";
import DashBoard from "./screens/dashBoard";

const headerStyle = { backgroundColor: "rgb(252,204,47)" };
const headerTint = "rgb(26,58,131)";
const mainApp = createStackNavigator(
  {
    DashBoard: {
      screen: DashBoard,
      navigationOptions: {
        headerStyle: headerStyle,
        headerTintColor: headerTint,
      },
    },
  },
  {
    initialRouteName: "DashBoard",
  }
);
const LoginRoute = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: () => ({
        headerTitleStyle: {
          fontSize: 20,
          fontWeight: "bold",
          color: "rgb(26,58,131)",
        },
        title: `Welcome to Appiness`,
        headerStyle: {
          backgroundColor: "rgb(252,204,47)",
        },
      }),
    },
    DashBoard: {
      screen: DashBoard,
      navigationOptions: {
        headerStyle: headerStyle,
        headerTintColor: headerTint,
      },
    },
  },
  {
    initialRouteName: "Login",
  }
);
export default createAppContainer(
  createSwitchNavigator(
    {
      App: mainApp,
      Auth: LoginRoute,
    },
    {
      initialRouteName: "Auth",
    }
  )
);
